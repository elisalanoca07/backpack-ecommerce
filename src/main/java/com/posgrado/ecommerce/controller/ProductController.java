package com.posgrado.ecommerce.controller;

import com.posgrado.ecommerce.dto.ProductDto;
import com.posgrado.ecommerce.entity.Product;
import com.posgrado.ecommerce.services.ProductService;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("/products")
public class ProductController {

  private ProductService productService;
  @PostMapping
  public ResponseEntity<Product> create (@RequestBody ProductDto productDto){
    Product product = productService.create(productDto);
    return ResponseEntity.status(HttpStatus.CREATED).body(product);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Product> update (@PathVariable UUID id, @RequestBody ProductDto productDto){
    Product product = productService.update(id, productDto);
    return ResponseEntity.status(HttpStatus.OK).body(product);
  }

  @GetMapping("/{id}")
  public ResponseEntity<Product> getById(@PathVariable UUID id){
    Product product = productService.getById(id);
    return ResponseEntity.status(HttpStatus.OK).body(product);
  }

  @GetMapping
  public ResponseEntity<List<Product>> getAll(){
    List<Product> products = productService.getAll();
    return ResponseEntity.status(HttpStatus.OK).body(products);
  }

  @GetMapping("/category/{id}")
  public ResponseEntity<Page<Product>> getAll(@PathVariable(name = "id") UUID categoryId, @RequestParam int page, @RequestParam int size){
    Pageable pageable = PageRequest.of(page, size);
    Page<Product> productPage = productService.findByCategory(categoryId, pageable);
    return ResponseEntity.status(HttpStatus.OK).body(productPage);
  }
}
