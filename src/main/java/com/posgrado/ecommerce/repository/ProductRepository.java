package com.posgrado.ecommerce.repository;

import com.posgrado.ecommerce.entity.Category;
import com.posgrado.ecommerce.entity.Product;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, UUID> {
//Optional<Role> findByName(String name);
  Optional<List<Product>> findByCategory(Category category);

  Page<Product> findByCategory(Category category, Pageable pageable);
}
