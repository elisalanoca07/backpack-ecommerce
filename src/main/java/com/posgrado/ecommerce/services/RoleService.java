package com.posgrado.ecommerce.services;

import com.posgrado.ecommerce.dto.RoleDto;
import com.posgrado.ecommerce.entity.Role;

public interface RoleService {
  Role getByName(String name);
  boolean existRole(String name);
  Role create(RoleDto roleDto);
}
