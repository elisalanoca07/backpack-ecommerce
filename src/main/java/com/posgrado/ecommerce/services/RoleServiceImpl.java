package com.posgrado.ecommerce.services;

import com.posgrado.ecommerce.dto.RoleDto;
import com.posgrado.ecommerce.entity.Role;
import com.posgrado.ecommerce.exception.RoleAlreadyRegistered;
import com.posgrado.ecommerce.mapper.RoleMapper;
import com.posgrado.ecommerce.repository.RoleRepository;
import com.posgrado.ecommerce.exception.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class RoleServiceImpl implements RoleService{

  private RoleRepository roleRepository;
  private RoleMapper roleMapper;

  @Override
  public Role getByName(String name) {
    return roleRepository.findByName(name).orElseThrow(()-> new EntityNotFoundException("Role not found"));
  }

  @Override
  public boolean existRole(String name) {
    return roleRepository.findByName(name).isPresent();
  }
  @Override
  public Role create(RoleDto roleDto) {
    boolean existRole = this.existRole(roleDto.getName());
    if (existRole) {
      throw new RoleAlreadyRegistered(roleDto.getName());
    }
    Role role = roleMapper.fromDto(roleDto);
    return roleRepository.save(role);
  }
}
