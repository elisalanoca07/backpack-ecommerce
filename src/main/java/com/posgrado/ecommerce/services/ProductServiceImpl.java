package com.posgrado.ecommerce.services;

import com.posgrado.ecommerce.entity.Category;
import com.posgrado.ecommerce.entity.Product;
import com.posgrado.ecommerce.repository.ProductRepository;
import com.posgrado.ecommerce.dto.ProductDto;
import com.posgrado.ecommerce.exception.EntityNotFoundException;
import com.posgrado.ecommerce.mapper.ProductMapper;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class ProductServiceImpl implements ProductService{

  private ProductRepository productRepository;
  private CategoryService categoryService;
  private ProductMapper productMapper;

  @Override
  public Product create(ProductDto productDto) {
    Category category = categoryService.getById(productDto.getCategoryId());

    Product product = productMapper.fromDto(productDto);
    product.setCategory(category);
    return productRepository.save(product);
  }

  @Override
  public Product update(UUID id ,ProductDto productDto) {
    Optional<Product> actualProdct = productRepository.findById(id);

    Category category = categoryService.getById(productDto.getCategoryId());

    Product product = productMapper.fromDto(productDto);
    product.setId(actualProdct.get().getId());
    product.setCategory(category);
    return productRepository.save(product);
  }

  @Override
  public Product getById(UUID id) {
    return productRepository.findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Product", id));
  }

  @Override
  public List<Product> getAll() {
    return productRepository.findAll();
  }

  @Override
  public Optional<List<Product>> findByCategory(UUID categoryId) {
    Category category = categoryService.getById(categoryId);
    return productRepository.findByCategory(category);
  }

  @Override
  public Page<Product> findByCategory(UUID categoryId, Pageable pageable) {
    Category category = categoryService.getById(categoryId);
    return productRepository.findByCategory(category, pageable);
  }
}
