package com.posgrado.ecommerce.services;

import com.posgrado.ecommerce.entity.Product;
import com.posgrado.ecommerce.dto.ProductDto;
import com.posgrado.ecommerce.entity.Role;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {
  Product create(ProductDto productDto);
  Product update(UUID id, ProductDto productDto);
  Product getById(UUID id);
  List<Product> getAll();
  Optional<List<Product>> findByCategory(UUID categoryId);
  Page<Product> findByCategory(UUID categoryId, Pageable pageable);
}
