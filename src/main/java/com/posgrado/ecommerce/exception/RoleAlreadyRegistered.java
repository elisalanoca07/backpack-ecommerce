package com.posgrado.ecommerce.exception;

public class RoleAlreadyRegistered extends RuntimeException {

  private static final String ERROR_MESSAGE = "Role with name %s already exists";

  public RoleAlreadyRegistered(String role) {
    super(String.format(ERROR_MESSAGE, role));
  }
}
