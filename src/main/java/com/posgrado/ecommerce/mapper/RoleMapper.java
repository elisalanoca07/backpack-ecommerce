package com.posgrado.ecommerce.mapper;

import com.posgrado.ecommerce.entity.Role;
import com.posgrado.ecommerce.dto.RoleDto;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper {

  public Role fromDto(RoleDto roleDto){
    Role role = new Role();
    role.setName(roleDto.getName());
    role.setDescription(roleDto.getDescription());
    return role;
  }
}
