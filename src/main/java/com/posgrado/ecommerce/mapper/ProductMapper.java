package com.posgrado.ecommerce.mapper;

import com.posgrado.ecommerce.dto.ProductDto;
import com.posgrado.ecommerce.entity.Product;
import org.springframework.stereotype.Component;

@Component
public class ProductMapper {

  public Product fromDto(ProductDto productDto){
    Product product = new Product();
    product.setName(productDto.getName());
    product.setDescription(productDto.getDescription());
    product.setImageUrl(productDto.getImageUrl());
    product.setActive(productDto.getActive());
    product.setPrice(productDto.getPrice());
    product.setStock(productDto.getStock());
    return product;
  }
}
